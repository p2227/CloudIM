ChatListUI = (function() {
	var my_id = null;
	var libase ={};
	var imchat_conv_id = null;
	var list_holder = null;
	
	function syncList(my_id) {
		Cloud.IM.syncConversationList(my_id, function(convs) {
			for (var c in convs) {
				var _conv = convs[c]
				if(database[_conv.conv_id]){
					continue;
				}
				database[_conv.conv_id] = _conv;
				
				insertTop(render(_conv));
				
			}
		})
	}
	function msgIncoming(pushPayload){
		App.log("msgIncoming " + JSON.stringify(pushPayload))
		var item; 
		var conv_id = pushPayload.msgPayload.conv_id
		if(database[conv_id]){
			item = findConvLi(conv_id);
			list_holder.removeChild(item);
			//TODO: 换成新的Info
		}
//		else{
			var conv = {
					conv_id: conv_id,
					my_id: my_id,
					latestMsg: pushPayload.msgPayload,
					peerUser: pushPayload.peerUser,
				}
			database[conv_id]= conv;
			item = render(conv);
			
//		}
		
		setUnread(item, "increase");
		insertTop(item);
		bindListTap();
		
	}
	function findConvLi(conv_id){
		return libase[conv_id];
	}
	

	function render(conv) {
		var data = {
			conv_id: conv.conv_id,
			photo: "../resource/photo/" + (conv.peerUser.userPhoto || "photo01.png"),
			name: 　conv.peerUser.userName || "no name",
			date: (conv.latestMsg && getTimeStamp(conv.latestMsg.timestamp)) || "",
			message: (conv.latestMsg&& conv.latestMsg.data) || "" ,
			unread: 0,
		}

		var div = document.createElement('div');
		div.innerHTML = template('msg-template', data);
		var li = div.querySelector('li');
		li.setAttribute("conv_id", conv.conv_id);
		libase[conv.conv_id]=li;
		
//		console.log(div.innerHTML )
		return li;
		
	}

	function setUnread(li, action){
		var badge_el = li.querySelector(".mui-badge");
		var n = 0;
		
		if(action == "increase"){
			n = parseInt(badge_el.innerHTML);
			n = (isNaN(n)?0:n)+1;
		}
		else if(action == "clear"){
			n= 0;
		}
		
		badge_el.innerHTML = n;
		if(n>0){
			badge_el.style.display = "inline";
		}
		else{
			badge_el.style.display = "none";
		}
	}
	function insertTop(li){
		list_holder.insertBefore(li, list_holder.firstElementChild);
//		console.log(li.getAttribute("conv_id"));
//		libase[li.getAttribute("conv_id")] = list_holder.firstElementChild;
	}

//	function getLatestMsg(msgS) {
//		var latestMsg;
//		if (msgS.length == 0) {
//			latestMsg = {
//				data: "",
//				timestamp: "",
//			}
//		} else {
//			latestMsg = msgS[0];
//		}
//		return latestMsg;
//	}

	function getTimeStamp(t) {
		var d = new Date(t);
		return d.toDateString();

	}

	function refresh(id) {
		my_id = id;
		libase = [];
		database = [];
		list_holder.innerHTML = "<li></li>";

		syncList(my_id);
		bindListTap();
	}
	function bindListTap(){
		mui("#list").off(TAP, '.mui-table-view-cell')

		mui("#list").on(TAP, '.mui-table-view-cell', function() {
			
			var conv_id = this.getAttribute("conv_id");
			showChatView(conv_id);
		})
	}
	function showChatView(conv_id){
		console.log("showChatView " +conv_id )
		console.log(JSON.stringify(database[conv_id]))
			mui.fire(ImChatView, "conv_list", {conv:JSON.stringify(database[conv_id])});
			imchat_conv_id = conv_id;
	}
	function pushMsgHandler(payload){
		msgIncoming(payload);
		console.log(imchat_conv_id)
		if(payload.msgPayload.conv_id == imchat_conv_id){
			mui.fire(ImChatView, "push_msg", {payload:JSON.stringify(payload)});
		}
	}
	

	function init(holder) {
		list_holder = holder;
		if(ImChatView){return;}
		
		ImChatViewId = "im-chat.html";
		ImChatView = mui.preload({
			url: "im-chat.html",
			id: ImChatViewId, //默认使用当前页面的url作为id
			styles: {}, //窗口参数
			extras: {previous_webview_id: current_webview_id} //自定义扩展参数
		});

		window.addEventListener("imchat:quit", function(event) {
			setUnread(findConvLi(imchat_conv_id), "clear");
			imchat_conv_id = null;
		});

	}


	return {
		init: init,
		refresh: refresh,
		pushMsg: msgIncoming,
		pushMsgHandler:  pushMsgHandler,
		
		render: render,
	}
}())
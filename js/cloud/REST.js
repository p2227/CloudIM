REST = (function($){
	var appId='2vqtxi2xvij6vb7xfempoze1bjvdgi1yum2e2xictilem1q9';
	var appKey='1o9rb4nwxijae6i68q28gvgtoov3zb2rj84ml35ye2lqqlo6';
	var masterKey = '7od1rzoaahay7a5a5nm2p5wbqy65b6mxpvgj35sj8fj1ilky'
		
		
	var default_success = function(response) {
//		if (dataType === 'json') {
//			response = JSON.stringify(response);
//		} else if (dataType === 'xml') {
//			response = new XMLSerializer().serializeToString(response).replace(/</g, "&lt;").replace(/>/g, "&gt;");
//		}
		console.log("success: " + JSON.stringify(response));
		return response;
	};
	//设置全局beforeSend
	$.ajaxSettings.beforeSend = function(xhr, setting) {
		//beforeSend演示,也可在$.ajax({beforeSend:function(){}})中设置单个Ajax的beforeSend
//		console.log('beforeSend:::' + JSON.stringify(setting));
	};
	//设置全局complete
	$.ajaxSettings.complete = function(xhr, status) {
//		console.log('complete:::' + status);
//		console.log('xhr:::' + JSON.stringify(xhr));
	}
	var LeanCloudAjax = function(url, data, type, success, master) {
		url = "https://api.leancloud.cn/1.1" + url;
		var dataType = 'json';
		success = success || default_success;
		
//		url = url + (dataType === 'html' ? 'text' : dataType);
		var heads = master? {
			"X-AVOSCloud-Application-Id":  appId,
			"X-AVOSCloud-Request-Sign": (function(){
					var d = new Date().getTime();
					var sign = md5(d + masterKey);
					return sign +"," +d +",master";

			}()),
			"Content-Type": 'application/json',
		}:{
			"X-AVOSCloud-Application-Id":  appId,
			"X-AVOSCloud-Application-Key": appKey,
			"Content-Type": 'application/json',
		};
		
//		$.get(url, data, success, dataType);
		
		$.ajax({
				url: url,
				type : type,
				headers: heads,
//				data: JSON.stringify(data),
				data: data,
				success: success,
				dataType: dataType,
			})
	};
	
	return {
		post: function(url, data, success, master){
			LeanCloudAjax(url, JSON.stringify(data), "POST", success, master);
		},
		get:function(url, data,success, master){
			LeanCloudAjax(url, data, "GET", success, master);
		},
		put:function(url, data, success, master){
			LeanCloudAjax(url, JSON.stringify(data), "PUT", success, master);
		},
		
		
		
	}
	
}(mui))
